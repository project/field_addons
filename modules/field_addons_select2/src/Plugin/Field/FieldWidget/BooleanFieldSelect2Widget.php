<?php

namespace Drupal\field_addons_select2\Plugin\Field\FieldWidget;

use Drupal\field_addons\Plugin\Field\FieldWidget\BooleanFieldSelectWidgetBase;

/**
 * Plugin implementation of the 'boolean_select2_list' widget.
 *
 * @FieldWidget(
 *   id = "field_addons_select2_bool",
 *   label = @Translation("Select2 list"),
 *   field_types = {
 *     "boolean"
 *   },
 *    multiple_values = FALSE
 * )
 */
class BooleanFieldSelect2Widget extends BooleanFieldSelectWidgetBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldType() {
    return 'select2';
  }

}
