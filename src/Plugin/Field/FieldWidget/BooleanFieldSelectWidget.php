<?php

namespace Drupal\field_addons\Plugin\Field\FieldWidget;

/**
 * Plugin implementation of the 'boolean_select_list' widget.
 *
 * @FieldWidget(
 *   id = "field_addons_select_bool",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "boolean"
 *   },
 *    multiple_values = FALSE
 * )
 */
class BooleanFieldSelectWidget extends BooleanFieldSelectWidgetBase {

  /**
   * {@inheritdoc}
   */
  protected function getFieldType() {
    return 'select';
  }

}
