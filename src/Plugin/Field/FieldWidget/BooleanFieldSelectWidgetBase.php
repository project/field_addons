<?php

namespace Drupal\field_addons\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for the boolean select widgets.
 *
 * Field widget could be used for the boolean fields and display it as a
 * select list.
 */
abstract class BooleanFieldSelectWidgetBase extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'empty_option_label' => t('N/A'),
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['empty_option_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Empty option label'),
      '#default_value' => $this->getSetting('empty_option_label'),
      '#description' => $this->t("Label for the empty select option, which will be displayed if no value is selected yet."),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Empty option label: @label', ['@label' => $this->getSetting('empty_option_label')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $options = $this->getOptions($items->getEntity());
    $selected = $this->getSelectedOptions($items);

    // If required and there is one single option, preselect it.
    if ($this->required && count($options) == 1) {
      reset($options);
      $selected = [key($options)];
    }

    $element += [
      '#type' => $this->getFieldType(),
      '#default_value' => $selected,
      '#options' => $options,
      '#key_column' => $this->column,
      '#empty_option' => $this->getSetting('empty_option_label'),
    ];
    $element['#element_validate'][] = [static::class, 'validateElement'];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    $form_state->setValueForElement($element, [$element['#key_column'] => $element['#value']]);
  }

  /**
   * Field type to display boolean field.
   *
   * @return string
   *   Returns field type.
   */
  protected function getFieldType() {
    return 'select';
  }

}
